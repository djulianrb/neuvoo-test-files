package com.example.julianrodriguez.neuvootestapplication;

import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class MyWebViewClient extends WebViewClient {

    ImageView imageView;

    public void setImageView(ImageView imageView){
        this.imageView = imageView;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        String direccion = Uri.parse(url).getPath().toString();
        if (direccion.equals("/page2.html")) {

            imageView.setEnabled(true);
            imageView.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }
}
