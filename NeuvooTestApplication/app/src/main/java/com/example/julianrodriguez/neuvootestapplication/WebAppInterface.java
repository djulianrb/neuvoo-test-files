package com.example.julianrodriguez.neuvootestapplication;

import android.app.Activity;
import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.TextView;

public class WebAppInterface {
    Context mContext;
    TextView textView;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }



    @JavascriptInterface   // must be added for API 17 or higher
    public void changeMenu(String value) {
        TextView txtView = (TextView) ((Activity)mContext).findViewById(R.id.textView);
        txtView.setText(value);
    }
}
